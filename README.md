# Send Email by using CRON Jobs

CRON Jobs are used for scheduling programs that runs after certain time period.
For example, taking database dump/backup, sending emails for marketing/monitoring purposes.

A simple Node.js application to send emails every minute using CRON Jobs.

## Install express.js

```javascript
npm install express --save
```
Simple program to create and run server using express.
```javascript
const express = require('express');
const app = express();

app.listen(3000, function() {
  console.log('Running on 3000!');
});
```
## Install nodemailer by npm

> Using `nodemailer` library for sending mail.

For more info on *_nodemailer_*, visit [here](https://nodemailer.com/about/).

Check my [repo](https://bitbucket.org/dana46/node_email_nodemailer/src) for using `nodemailer`.

```javascript
npm install nodemailer --save
```
## Creating a transporter using nodemailer

> `nodemailer` comes with a transporter, which binds our email configuration and makes it ready to be used to trigger emails.

```javascript
let transporter = nodemailer.createTransport({
  host : 'smtp.gmail.com',
  port : 465,
  secure : true,
  auth : {
    user : 'exampleFrom@gmail.com',
    pass : 'examplePassword'
  }
});
```

## Building mailOptions

```javascript
let mailOptions = {
  from    : 'exampleFrom@gmail.com',
  to      : 'exampleTo@gmail.com',
  subject : 'Test Mail!',
  content : 'Hello World!',
};
```

## Install node-cron by npm

```javascript
npm install node-cron --save
```

For more info on *_node-cron_*, visit [here](https://www.npmjs.com/package/node-cron).

Include it in the application.
```javascript
const cron = require('node-cron');
```

## Basics of scheduling cron job

```javascript
 # ┌────────────── second (optional)
 # │ ┌──────────── minute
 # │ │ ┌────────── hour
 # │ │ │ ┌──────── day of month
 # │ │ │ │ ┌────── month
 # │ │ │ │ │ ┌──── day of week
 # │ │ │ │ │ │
 # │ │ │ │ │ │
 # * * * * * *
```

## Scheduling the job to send email every minute

```javascript
cron.schedule('* * * * *', function() {
  console.log('Heart Beat!');

  transporter.sendMail(mailOptions, (err, info) => {
    if(err) {
      return console.log(err);
    }
    if(info) {
      console.log('Successful!');
    }
  });
});
```