const express = require('express');
const app = express();
const cron = require('node-cron'); // node-cron
const nodemailer = require('nodemailer');

// transporter set
let transporter = nodemailer.createTransport({
  host : 'smtp.gmail.com',
  port : 465,
  secure : true,
  auth : {
    user : 'exampleFrom@gmail.com',
    pass : 'examplePassword'
  }
});

// mailOptions set
let mailOptions = {
  from    : 'exampleFrom@gmail.com',
  to      : 'exampleTo@gmail.com',
  subject : 'Test Mail!',
  content : 'Hello World!',
};

// cron job scheduled to send mail every minute
cron.schedule('* * * * *', function() {
  console.log('Heart Beat!');

  transporter.sendMail(mailOptions, (err, info) => {
    if(err) {
      return console.log(err);
    }
    if(info) {
      console.log('Successful!');
    }
  });
});

app.listen(3000, function() {
  console.log('Running in 3000!');
});